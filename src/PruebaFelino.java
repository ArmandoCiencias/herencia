/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tlahuizcalpan
 */
public class PruebaFelino {
    public static void main(String[] args) {
        Felino[] felinos = new Felino[10];
        
        for (int i = 0; i < felinos.length; i++) {
            if(Math.random() <= 0.5)
                felinos[i] = new Gato();
            else
                felinos[i] = new Tigre();
        }
        
        for (Felino felino : felinos) {
            felino.ronronear();
            
            if(felino instanceof Gato)
                ((Gato)felino).maullar();
            else
                ((Tigre)felino).rugir();
        }
    }
}
