/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tlahuizcalpan
 */
public class Gato extends Felino {

    public Gato() {
        super(Garra.crearGarras(15));
    }

    public Gato(Garra[] garras) {
        super(garras);
    }
    
    public void maullar() {
        System.out.println("miau!");
    }

    @Override
    public void ronronear() {
        System.out.println("rrrrrrRRrrRrRrRRrRrrRrrrrrrr!! Soy Gato");
    }

    @Override
    public String toString() {
        return "Gato: " + super.toString();
    }
    
    public static void main(String[] args) {
        Gato gato = new Gato();
        gato.ronronear();
        gato.maullar();
        System.out.println(gato.getGarras().length);
        
        System.out.println(gato);
        
        gato.cazar(new Presa());
    }
}
