/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tlahuizcalpan
 */
public abstract class Felino {
    private Garra[] garras;//null

    public Felino() {
        this.garras = Garra.crearGarras(20);
    }

    public Felino(Garra[] garras) {
        this.garras = garras;
    }

    public Garra[] getGarras() {
        return garras;
    }
    
    public void cazar(Presa presa) {
        presa.morir();
    }
    
    public abstract void ronronear();
    
    private int calcularGarrasEnBuenEstado() {
        int garritas = 0;
        for (Garra garra : garras) {
            if(!garra.isRota())
                garritas++;
        }
        
        return garritas;
    }

    @Override
    public String toString() {
        return String.format(
                "Felino con %s garras en buen estado", 
                calcularGarrasEnBuenEstado());
    }
    
//    public static void main(String[] args) {
//        Felino f = new Felino();
        
//        for (int i = 0; i < f.getGarras().length; i++) {
//            Garra garra = f.getGarras()[i];
//            
//            System.out.println(garra);
//        }
//        
//        System.out.println("Garras f");
//        for (Garra garra: f.getGarras()) {
//            System.out.println(garra);
//        }
//        
//        Garra[] garras = Garra.crearGarras(20);
//        for (int i = 4; i < 10; i++) {
//            garras[i].setRota(true);
//        }
//        
//        Felino f2 = new Felino(garras);
//        System.out.println("");
//        System.out.println("Garras f");
//        for (int i = 0; i < f2.getGarras().length; i++) {
//            System.out.println(f2.getGarras()[i]);
//        }
//    }
}
