
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tlahuizcalpan
 */
public class Garra {
    private boolean rota;

    public boolean isRota() {
        return rota;
    }

    public void setRota(boolean rota) {
        this.rota = rota;
    }

    @Override
    public String toString() {
        return "garra " + (rota ? "rota": "en buen estado");
    }
    
    public static Garra[] crearGarras(int numeroGarras) {
        Garra[] garras = new Garra[numeroGarras];
        
        for (int i = 0; i < garras.length; i++) {
            garras[i] = new Garra();
        }
        
        return garras;
    }
    
    public static void main(String[] args) {
        Garra[] garras = Garra.crearGarras(5);
        garras[2].setRota(true);
        garras[3].setRota(true);
        
        for (Garra garra : garras) {
            System.out.println(garra);
        }
    }
}
